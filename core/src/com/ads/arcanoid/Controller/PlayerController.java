package com.ads.arcanoid.Controller;

import com.ads.arcanoid.ArcanoidGame;
import com.ads.arcanoid.Model.World;
import com.ads.arcanoid.Model.Player;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputProcessor;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;

import static com.badlogic.gdx.Input.Keys.A;
import static com.badlogic.gdx.Input.Keys.D;
import static com.badlogic.gdx.Input.Keys.LEFT;
import static com.badlogic.gdx.Input.Keys.RIGHT;

/**
 * Created by ga_nesterchuk on 17.02.2016.
 */
public class PlayerController implements InputProcessor {

    Player player;
    HashSet<Integer> pressedKeys;
    ArrayList<Character> printedChars;
    HashMap<Integer, Pointer> touchedPointers;
    ControlType control;

    public PlayerController(World world){
        player = world.getPlayer();
        pressedKeys = new HashSet<Integer>();
        printedChars = new ArrayList<Character>();
        touchedPointers = new HashMap<Integer, Pointer>();
        control = ArcanoidGame.getInstance().getControlType();
    }

    public void updatePlayer(){
        player.setDirection(getCurrentDirection());
    }

    public Direction getCurrentDirection(){
        switch (control){
            case BUTTONS:
                if (movesLeft() && !movesRight()) return Direction.LEFT;
                if (!movesLeft() && movesRight()) return Direction.RIGHT;
                break;
            case TOUCH:
                if (movesLeft() && !movesRight()) return Direction.LEFT;
                if (!movesLeft() && movesRight()) return Direction.RIGHT;
                break;
        }
        return Direction.NONE;
    }

    private boolean movesLeft(){
        switch (control){
            case BUTTONS:
                return pressedKeys.contains(A) || pressedKeys.contains(LEFT);
            case TOUCH:
                if (!touchedPointers.isEmpty()){
                    Pointer p = touchedPointers.values().iterator().next();
                    if (2*p.getX() <= Gdx.graphics.getWidth())
                        return true;
                }
                break;
        }
        return false;
    }
    private boolean movesRight(){
        switch (control){
            case BUTTONS:
                return pressedKeys.contains(D) || pressedKeys.contains(RIGHT);
            case TOUCH:
                if (!touchedPointers.isEmpty()){
                    Pointer p = touchedPointers.values().iterator().next();
                    if (2*p.getX() > Gdx.graphics.getWidth())
                        return true;
                }
                break;
        }
        return false;
    }

    @Override
    public boolean keyDown(int keycode) {
        return pressedKeys.add(keycode);
    }

    @Override
    public boolean keyUp(int keycode) {
        return pressedKeys.remove(keycode);
    }

    @Override
    public boolean keyTyped(char character) {
        return printedChars.add(character);
    }

    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {
        if (!touchedPointers.containsKey(pointer)) {
            touchedPointers.put(pointer, new Pointer(screenX, screenY, button));
            return true;
        }
        return false;
    }

    @Override
    public boolean touchUp(int screenX, int screenY, int pointer, int button) {
        if (touchedPointers.containsKey(pointer)) {
            touchedPointers.remove(pointer);
            return true;
        }
        return false;
    }

    @Override
    public boolean touchDragged(int screenX, int screenY, int pointer) {
        if (touchedPointers.containsKey(pointer)) {
            touchedPointers.get(pointer).setCoords(screenX, screenY);
            return true;
        }
        return false;
    }

    @Override
    public boolean mouseMoved(int screenX, int screenY) {
        return false;
    }

    @Override
    public boolean scrolled(int amount) {
        return false;
    }
}

class Pointer{
    private int x;
    private int y;
    private int button;
    public Pointer(int x, int y, int button){
        this.button = button;
        setCoords(x, y);
    }
    public void setCoords(int x, int y){
        this.x = x;
        this.y = y;
    }
    public int getX(){
        return x;
    }
    public int getY(){
        return y;
    }
}
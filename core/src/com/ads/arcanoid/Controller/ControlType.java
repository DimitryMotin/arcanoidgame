package com.ads.arcanoid.Controller;

/**
 * Created by ga_nesterchuk on 17.02.2016.
 */
public enum ControlType {
    TILT,
    BUTTONS,
    TOUCH,
    NONE
}

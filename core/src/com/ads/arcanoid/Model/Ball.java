package com.ads.arcanoid.Model;

import com.ads.arcanoid.ArcanoidGame;
import com.ads.arcanoid.Model.World;
import com.ads.arcanoid.View.ImageActor;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

/**
 * Created by ga_nesterchuk on 10.02.2016.
 */
public class Ball extends ImageActor {
    private float step;
    private float angle;
    private Player player;

    public Ball(TextureRegion img, float x, float y, float width, float height, float step, float angle) {
        super(img, x, y, width, height);
        this.step = step;
        this.angle = angle;
    }

    public Ball(Texture img, float x, float y, float step, float angle) {
        super(img, x, y);
        this.step = step;
        this.angle = angle;
    }

    public Ball(Texture img, float x, float y, float width, float height, float step, float angle) {
        super(img, x, y, width, height);
        this.step = step;
        this.angle = angle;
    }

    public Ball(TextureRegion img, float x, float y, float step, float angle) {
        super(img, x, y);
        this.step = step;
        this.angle = angle;
    }

    public void act(float delta) {
        float radAngle = (float) Math.PI * angle / 180f;
        moveBy((float) Math.cos(radAngle) * delta * step, (float) Math.sin(radAngle) * delta * step);
        if (getX() < 0) {
            setX(0);
            angle = 180 - angle;
        }
        if (getX() + getWidth() > 920) {
            setX(920 - getWidth());
            angle = 180 - angle;
        }
        if (getY() + getHeight() > 540) {
            setY(540 - getHeight());
            angle = -angle;
        }
        player = ((World) getStage()).getPlayer();
        if (getY() < player.getHeight()) {
            float tmp = getX() - player.getX();

            if (tmp < player.getWidth() && tmp > 0) {
                setY(player.getHeight());
                angle = 180*(float)(player.getWidth()-tmp)/(float)(player.getWidth());
            }
        }
        if (getY()<0)
            ArcanoidGame.getInstance().moveToMainMenu();

    }


}

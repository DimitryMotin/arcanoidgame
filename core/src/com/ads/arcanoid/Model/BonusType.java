package com.ads.arcanoid.Model;

/**
 * Created by ga_nesterchuk on 17.02.2016.
 */
public enum BonusType {
    EXTRALIFE,
    SPEEDUP,
    SPEEDDOWN
}

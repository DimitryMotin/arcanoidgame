package com.ads.arcanoid.Screens;

import com.ads.arcanoid.Controller.PlayerController;
import com.ads.arcanoid.Model.World;
import com.ads.arcanoid.View.ImageActor;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.viewport.ScreenViewport;

import java.util.HashMap;

/**
 * Created by Гриша on 27.01.2016.
 */
public class GameScreen implements Screen {
    private World world;
    private InputMultiplexer multiplexer;
    private PlayerController controller;

    public GameScreen(SpriteBatch batch, HashMap<String, TextureRegion> textureRegions) {
        OrthographicCamera camera = new OrthographicCamera();
        camera.setToOrtho(false, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        world = new World(new ScreenViewport(camera), batch, textureRegions);
        controller = new PlayerController(world);
        multiplexer = new InputMultiplexer();
        multiplexer.addProcessor(world);
        multiplexer.addProcessor(controller);
    }

    @Override
    public void show() {
        world.reset();
        Gdx.input.setInputProcessor(multiplexer);
    }

    @Override
    public void render(float delta) {
        Gdx.gl20.glClear(GL20.GL_COLOR_BUFFER_BIT);
        controller.updatePlayer();
        world.act(delta);
        world.draw();
    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {
        world.dispose();
        Gdx.input.setInputProcessor(null);

    }
}

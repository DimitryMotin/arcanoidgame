package com.ads.arcanoid.Screens;

import com.ads.arcanoid.Controller.GoToGame;
import com.ads.arcanoid.Controller.GoToSettingsMenu;
import com.ads.arcanoid.View.ImageActor;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.utils.viewport.ScreenViewport;
import com.badlogic.gdx.utils.viewport.Viewport;

import java.util.HashMap;

/**
 * Created by Гриша on 27.01.2016.
 */
public class MainMenuScreen implements Screen {
    private ImageActor background;
    private ImageActor logo;
    private ImageActor settingsButton;
    private ImageActor playButton;
    private Stage stage;

    public MainMenuScreen(SpriteBatch batch, HashMap<String, TextureRegion> textureRegions) {
        OrthographicCamera camera = new OrthographicCamera();
        camera.setToOrtho(false, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        stage = new Stage(new ScreenViewport(camera), batch);

        background = new ImageActor(new Texture("android/assets/background.png"), 0, 0);
        logo = new ImageActor(new Texture("android/assets/logo.png"), 105, 380);
        playButton = new ImageActor(new Texture("android/assets/play-btn.png"), 353, 230);
        playButton.addListener(new GoToGame());
        settingsButton = new ImageActor(new Texture("android/assets/settings-btn.png"), 259, 70);
        settingsButton.addListener(new GoToSettingsMenu());

        stage.addActor(background);
        stage.addActor(logo);
        stage.addActor(playButton);
        stage.addActor(settingsButton);
    }

    @Override
    public void show() {
        Gdx.input.setInputProcessor(stage);
    }

    @Override
    public void render(float delta) {
        Gdx.gl20.glClear(GL20.GL_COLOR_BUFFER_BIT);
        stage.act(delta);
        stage.draw();
    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {

    }
}

package com.ads.arcanoid.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.ads.arcanoid.ArcanoidGame;

public class DesktopLauncher {
	public static void main (String[] arg) {
		System.setProperty("user.name","EnglishWords");
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
		config.width = 920;
		config.height = 540;
		new LwjglApplication(ArcanoidGame.getInstance(), config);
	}
}
